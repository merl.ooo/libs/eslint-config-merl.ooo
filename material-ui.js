module.exports = {
  plugins: ['material-ui'],
  rules: {
    'no-restricted-imports': [
      'error',
      {
        patterns: ['@mui/*/*/*', '!@mui/material/test-utils/*'],
      },
    ],
  },
};
