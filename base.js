module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
    es2022: true,
  },
  extends: [
    'eslint:recommended',
    'airbnb',
    'plugin:promise/recommended',
    'plugin:unicorn/recommended',
  ],
  rules: {
    quotes: ['error', 'single'],
    'class-methods-use-this': 'off',
    'no-useless-constructor': 'off',
    'lines-between-class-members': 'off',
    'unicorn/no-null': 'off',
    'unicorn/consistent-function-scoping': 'off',
  },
};
