# eslint-config-merl.ooo

merl.ooo eslint config comes with a `base` and a `react` style config.
Additionally you can add the `typescript` and `material-ui` config.

## Usage

### Base

Install package and eslint plugins:

```
npm i -D eslint eslint-config-merl.ooo eslint-config-airbnb eslint-plugin-promise eslint-plugin-unicorn
```

Add these lines to your `package.json`:
```json
"eslintConfig": {
  "extends": "merl.ooo"
}
```

### React

Install package, eslint plugins and additional eslint react dependencies:

```
npm i -D eslint eslint-config-merl.ooo eslint-config-airbnb eslint-plugin-promise eslint-plugin-unicorn
npm i -D eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-react-hooks
```

Add these lines to your `package.json`:
```json
"eslintConfig": {
  "extends": [
    "merl.ooo",
    "merl.ooo/react"
  ],
}
```

### Typescript

Install the [base](#base) or [react](#React) dependencies.

Additionally install all typescript eslint dependencies:
```
npm i -D @typescript-eslint/eslint-plugin @typescript-eslint/parser eslint-config-airbnb-typescript
```

Add these lines to your `package.json`:
```json
"eslintConfig": {
  "extends": [
    "merl.ooo",
    "merl.ooo/react", // if react project
    "merl.ooo/typescript"
  ],
  "parserOptions": {
    "project": "./tsconfig.json",
  },
}
```

### Material-UI

Install all eslint [react](#React) dependencies.
Optionally install the eslint [typescript](#Typescript) dependencies.

Install the eslint-plugin-material-ui:
`npm i -D eslint-plugin-material-ui`

Add these lines to your `package.json`:
```json
"eslintConfig": {
  "extends": [
    "merl.ooo",
    "merl.ooo/react", // if react project
    "merl.ooo/typescript", // if typescript project
    "merl.ooo/material-ui"
  ],
  "parserOptions": { // if typescript project
    "project": "./tsconfig.json",
  },
}
```

## License

MIT
